-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: sql11.freemysqlhosting.net
-- Generation Time: Apr 11, 2021 at 09:40 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sql11404879`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `brand` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `consumption` float DEFAULT NULL,
  `notes` text COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `active`, `brand`, `model`, `type`, `consumption`, `notes`) VALUES
(5, 1, 'Škoda', 'Octavia', 'combi', 6.1, 'rok výroby 2020, nájezd 560km, perfektní stav'),
(6, 1, 'Fiat', 'Panda', 'combi', 5.4, 'rok vyroby 2017, 4x4'),
(7, 1, 'Renault', 'Meghane', 'sedan', 5.9, 'rok výroby 2019, lehce otlučené zezadu'),
(8, 1, 'Suzuki', 'Swift', 'combi', 5, 'rok výroby 2015, modrá barva');

-- --------------------------------------------------------

--
-- Stand-in structure for view `cars_reservations`
-- (See below for the actual view)
--
CREATE TABLE `cars_reservations` (
`rs_active` tinyint(1)
,`id` int(11)
,`user_id` int(11)
,`begin` datetime
,`end` datetime
,`brand` varchar(255)
,`type` varchar(255)
,`notes` text
,`car_active` tinyint(1)
,`full_name` varchar(255)
,`car_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `cars_service`
--

CREATE TABLE `cars_service` (
  `id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `repair_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `repair_desc` text COLLATE utf8mb4_bin,
  `time` datetime DEFAULT NULL,
  `invoice` int(11) DEFAULT NULL,
  `cost` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `time`) VALUES
(1, 4, '2021-04-09 16:52:40'),
(2, 4, '2021-04-09 16:54:17'),
(3, 4, '2021-04-09 21:14:26'),
(4, 4, '2021-04-09 21:15:40'),
(5, 4, '2021-04-09 21:16:39'),
(6, 4, '2021-04-09 21:44:00'),
(7, 4, '2021-04-09 21:46:42'),
(8, 1, '2021-04-09 21:47:33'),
(9, 4, '2021-04-09 21:55:15'),
(10, 4, '2021-04-09 21:56:47'),
(11, 4, '2021-04-09 21:57:29'),
(12, 4, '2021-04-09 21:57:47'),
(13, 4, '2021-04-09 22:03:49'),
(14, 4, '2021-04-09 22:04:33'),
(15, 4, '2021-04-09 22:05:08'),
(16, 1, '2021-04-09 22:05:18'),
(17, 4, '2021-04-09 22:06:16'),
(18, 4, '2021-04-09 22:06:33'),
(19, 1, '2021-04-09 22:06:37'),
(20, 1, '2021-04-09 22:07:08'),
(21, 1, '2021-04-09 22:07:43'),
(22, 4, '2021-04-09 22:08:09'),
(23, 4, '2021-04-10 11:01:15'),
(24, 4, '2021-04-10 11:08:15'),
(25, 1, '2021-04-10 11:08:49'),
(26, 4, '2021-04-10 11:10:15'),
(27, 1, '2021-04-10 11:10:22'),
(28, 1, '2021-04-10 11:13:12'),
(29, 1, '2021-04-10 11:13:51'),
(30, 1, '2021-04-10 11:14:00'),
(31, 4, '2021-04-10 11:16:12'),
(32, 4, '2021-04-10 11:17:53'),
(33, 4, '2021-04-10 11:26:00'),
(34, 1, '2021-04-10 11:26:11'),
(35, 1, '2021-04-10 11:26:40'),
(36, 4, '2021-04-10 11:49:50'),
(37, 4, '2021-04-10 11:50:36'),
(38, 4, '2021-04-10 11:53:14'),
(39, 4, '2021-04-10 12:00:20'),
(40, 4, '2021-04-10 12:16:50'),
(41, 4, '2021-04-10 12:18:16'),
(42, 4, '2021-04-10 12:20:32'),
(43, 4, '2021-04-10 12:21:56'),
(44, 1, '2021-04-10 19:25:44'),
(45, 1, '2021-04-10 19:32:58'),
(46, 4, '2021-04-10 19:33:05'),
(47, 4, '2021-04-10 19:38:53'),
(48, 4, '2021-04-10 19:42:34'),
(49, 4, '2021-04-10 19:45:07'),
(50, 4, '2021-04-10 19:51:44'),
(51, 4, '2021-04-10 19:52:21'),
(52, 4, '2021-04-10 20:00:56'),
(53, 4, '2021-04-10 20:02:17'),
(54, 4, '2021-04-10 20:04:22'),
(55, 4, '2021-04-10 20:05:23'),
(56, 4, '2021-04-10 20:08:43'),
(57, 4, '2021-04-10 20:28:37'),
(58, 4, '2021-04-10 20:32:20'),
(59, 15, '2021-04-10 20:33:00'),
(60, 4, '2021-04-10 20:33:11'),
(61, 15, '2021-04-10 20:34:30'),
(62, 4, '2021-04-10 20:35:21'),
(63, 15, '2021-04-10 20:44:40'),
(64, 16, '2021-04-10 20:59:24'),
(65, 15, '2021-04-10 21:00:15'),
(66, 15, '2021-04-10 21:02:00'),
(67, 16, '2021-04-10 21:07:18'),
(68, 15, '2021-04-10 21:07:51'),
(69, 17, '2021-04-10 21:11:16'),
(70, 4, '2021-04-10 21:13:03'),
(71, 4, '2021-04-10 21:14:16'),
(72, 15, '2021-04-11 10:11:47'),
(73, 15, '2021-04-11 10:12:58'),
(74, 15, '2021-04-11 10:17:53'),
(75, 15, '2021-04-11 10:18:13'),
(76, 15, '2021-04-11 10:18:24'),
(77, 16, '2021-04-11 10:18:37'),
(78, 16, '2021-04-11 10:20:44'),
(79, 15, '2021-04-11 10:27:05'),
(80, 15, '2021-04-11 10:38:28'),
(81, 15, '2021-04-11 10:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `begin` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `notes` text COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `user_id`, `car_id`, `active`, `begin`, `end`, `notes`) VALUES
(17, 16, 8, 1, '2021-04-11 20:59:24', '2021-04-13 00:00:00', NULL),
(18, 16, 7, 0, '2021-04-21 20:59:24', '2021-04-22 00:00:00', NULL),
(19, 15, 8, 1, '2021-04-15 21:03:04', '2021-04-16 00:00:00', NULL),
(20, 16, 8, 1, '2021-04-20 21:07:29', '2021-04-21 00:00:00', NULL),
(21, 17, 7, 1, '2021-04-11 21:11:16', '2021-04-12 00:00:00', NULL),
(22, 15, 6, 1, '2021-05-01 11:39:32', '2021-05-30 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `nickname` varchar(127) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `identification` int(11) DEFAULT NULL,
  `driving_licence` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `new_password` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `active`, `admin`, `nickname`, `email`, `full_name`, `identification`, `driving_licence`, `created_at`, `password`, `new_password`) VALUES
(1, 0, 0, 'luky', '', '', 0, 0, '2021-04-03 16:05:27', '', 0),
(4, 0, 1, '.', '', '', 0, 0, '2021-04-03 17:10:36', '', 0),
(11, 1, 0, 'carka', 'a@a.cz', '.', 0, 0, '2021-04-10 12:22:14', '$MYHASH$V1$10000$ThGCb4F/d9hcOnqdeYk8qHNQtBNM6w/GOueQPtTd7ajzk7Gz', 0),
(15, 1, 1, 'admin', 'admin@vozovypark.cz', 'admin', 1, 1, '2021-04-10 20:32:42', '$MYHASH$V1$10000$Pakj/fRWDVDqPxqd2woVmarZBXzkwUBkr1f4r6L4/t7oc39d', 0),
(16, 1, 0, 'sedivka', 'sedivka@email.cz', 'Marek Sedy', 2147483647, 37619, '2021-04-10 20:59:13', '$MYHASH$V1$10000$4z+SYDtxfSxWwJYPECuRYWtHliKJLkuPUFyQN88UwRS9mee5', 0),
(17, 1, 0, 'cervena', 'cervinka@seznam.cz', 'Lucie Cervena', 689745321, 1269874, '2021-04-10 21:10:45', '$MYHASH$V1$10000$NrZK3d/uAM2p+QPAJ/dJ/4jIOrFpenD+enX25PhxrINtQsvM', 0),
(18, 1, 0, 'servis', 'servis@vozovypark.cz', 'servis', 0, 0, '2021-04-11 10:13:31', '$MYHASH$V1$10000$msB/UVg4gssI+Q4qFLCbcNsH9FQWYLjZEczQ7RPY84VTkuG1', 0);

-- --------------------------------------------------------

--
-- Structure for view `cars_reservations`
--
DROP TABLE IF EXISTS `cars_reservations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`sql11404879`@`%` SQL SECURITY DEFINER VIEW `cars_reservations`  AS  select `reservations`.`active` AS `rs_active`,`reservations`.`id` AS `id`,`reservations`.`user_id` AS `user_id`,`reservations`.`begin` AS `begin`,`reservations`.`end` AS `end`,`cars`.`brand` AS `brand`,`cars`.`type` AS `type`,`reservations`.`notes` AS `notes`,`cars`.`active` AS `car_active`,`users`.`full_name` AS `full_name`,`cars`.`id` AS `car_id` from ((`reservations` left join `cars` on((`reservations`.`car_id` = `cars`.`id`))) left join `users` on((`reservations`.`user_id` = `users`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars_service`
--
ALTER TABLE `cars_service`
  ADD PRIMARY KEY (`id`,`car_id`),
  ADD KEY `car_id` (`car_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`,`user_id`,`car_id`),
  ADD KEY `car_id` (`car_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nickname` (`nickname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cars_service`
--
ALTER TABLE `cars_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cars_service`
--
ALTER TABLE `cars_service`
  ADD CONSTRAINT `cars_service_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservations_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
