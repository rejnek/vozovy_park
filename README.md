# vozovy park

Lukáš Rejnek 3K, SSPŠ

## Obsah

Repozitář obsahuje můj čtvrtletní školní projekt a to program na správu vozového parku vytvořenou ve Windows Forms.

## Použití

Pro použití je potřeba být připojený k internetu. Databáze je k dispozici on-line a měla by fungovat bez nastavování. Využívám jákysi Free hosting, takže je možné se setkat s latencí příkazů. Program by měl být dostatešně intuitivní, aby ho běžný uživatel zvládnul použít bez nastavování.

## Přihlašovací údaje

Pro otestování funkčnosti jsou tady přihlašovací údaje.

```
nickname:   admin
heslo:      heslo
```
(má administrátorská práva)

```
nickname:   sedivka
heslo:      heslo
```
(nemá administrátorská práva)

Ostatní uživatelé mají také heslo `heslo` a můžete si je vypsat pomocí administrátorského účtu.

## Rezervace vozidla pro servis

Rezervace vozidla pro servis se provadí pomocí uživatele `servis`, který má administratorská prava a může zrušit rezervace uživatelů, které by stály v cestě.

```
nickname:   servis
heslo:      heslo
```

