﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class Cars : Form
    {
        private MySqlConnection _conn;
        private List<int> _cars;
        private User _u;

        public Cars(User u)
        {
            InitializeComponent();
            _cars = new List<int> { };
            _u = new User(u);
        }

        private void Cars_Load(object sender, EventArgs e)
        {
            _conn = ((Base) this.ParentForm)?.Conn;

            // ziskani vsech aut
            var sql = "SELECT * FROM `cars` WHERE `cars`.`active`";
            MySqlDataReader rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do listboxu
            while (rdr.Read())
            {
                lsBoxAvailable.Items.Add($"{rdr[2]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}");
                _cars.Add((int)rdr[0]);
            }
            rdr.Close();
        }

        private void btnNewCar_Click(object sender, EventArgs e)
        {
            // --
            // moznost pridat kontrolu vstupu
            // --
            string sql = $"INSERT INTO `cars` (`id`, `active`, `brand`, `model`, `type`, `consumption`, `notes`) VALUES (NULL, '1', '{txtBoxBrand.Text}', '{txtBoxModel.Text}', '{txtBoxType.Text}', '{nmrConsump.Value}', '{txtBoxNotes.Text}')";
            new MySqlCommand(sql, _conn).ExecuteNonQuery();

            // uklizeni po operaci
            ((Base)this.ParentForm)?.ToForm(new Cars(_u));
            MessageBox.Show("Auto uspesne pridane do databaze.");
        }

        private void lsBoxAvailable_DoubleClick(object sender, EventArgs e)
        {
            if (lsBoxAvailable.SelectedItem != null)
            {
                // otevreni noveho okna Car Info
                ((Base)this.ParentForm)?.ToForm(new CarInfo(_cars[lsBoxAvailable.SelectedIndex], _u));
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lsBoxAvailable.SelectedItem != null)
            {
                var sql = $"UPDATE `cars` SET `active` = '0' WHERE `cars`.`id` = '{_cars[this.lsBoxAvailable.SelectedIndex]}'";
                new MySqlCommand(sql, _conn).ExecuteNonQuery();
                
                // uklizeni po operaci
                ((Base) this.ParentForm)?.ToForm(new Cars(_u));
                MessageBox.Show("Auto smazano.");
            }
        }
    }
}
