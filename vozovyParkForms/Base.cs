﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class Base : Form
    {
        public User User;
        public MySqlConnection Conn;
        
        public Base()
        {
            InitializeComponent();
            User = new User();
        }

        public void NewMenu(User user)
        {
            this.pnlMenu.Controls.Clear();
            this.User = new User(user);
            Form m = new Menu(this.User) { TopLevel = false, TopMost = true };
            this.pnlMenu.Controls.Add(m);
            m.Show();
        }
        public void ToForm(Form f)
        {
            this.pnlForm.Controls.Clear();
            f.TopLevel = false;
            f.TopMost = true;
            this.pnlForm.Controls.Add(f);
            f.Show();
        }

        private void Base_Load(object sender, EventArgs e)
        {
            Conn = Db.Connect();
            this.ToForm(new Login());
        }
    }
}
