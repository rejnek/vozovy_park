﻿using System;
using System.Windows.Forms;

namespace vozovyParkForms
{
    public partial class Menu : Form
    {
        private Button[] _adminButtons;
        private User u;

        public Menu(User u)
        {
            InitializeComponent();
            this.u = new User(u);
            this.lblName.Text = this.u.fullName;
            this._adminButtons = new[] {this.btnUsers, this.btnCars};
            if (this.u.loggedIn == false)
            {
                this.pnlMenu.Hide();
            }
            else
            {
                this.pnlMenu.Show();
                if (this.u.isAdmin) return;
                foreach (var b in _adminButtons)
                {
                    b.Hide();
                }
            }
        }
        private void Menu_Load(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            ((Base) this.ParentForm)?.ToForm(new Home(u));
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            ((Base) this.ParentForm)?.ToForm(new Users());
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            ((Base) this.ParentForm)?.ToForm(new UserInfo(this.u.id));
        }

        private void btnCars_Click(object sender, EventArgs e)
        {
            ((Base) this.ParentForm)?.ToForm(new Cars(u));
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            ((Base) this.ParentForm)?.ToForm(new Login());
            ((Base) this.ParentForm)?.NewMenu(new User());
        }
        public void HideControl()
        {
            this.pnlMenu.Hide();
        }
    }
}