﻿
namespace vozovyParkForms
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblReservations = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lsBoxAvailable = new System.Windows.Forms.ListBox();
            this.lsBoxMy = new System.Windows.Forms.ListBox();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnReserve = new System.Windows.Forms.Button();
            this.comBoxCar = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comBoxUser = new System.Windows.Forms.ComboBox();
            this.pnlUserReservations = new System.Windows.Forms.Panel();
            this.btnFuture = new System.Windows.Forms.Button();
            this.btnHistory = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.pnlUserReservations.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblReservations
            // 
            this.lblReservations.AutoSize = true;
            this.lblReservations.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.lblReservations.Location = new System.Drawing.Point(12, 345);
            this.lblReservations.Name = "lblReservations";
            this.lblReservations.Size = new System.Drawing.Size(145, 25);
            this.lblReservations.TabIndex = 4;
            this.lblReservations.Text = "Moje rezervace:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Dostupné rezervace";
            // 
            // lsBoxAvailable
            // 
            this.lsBoxAvailable.Font = new System.Drawing.Font("Consolas", 9F);
            this.lsBoxAvailable.FormattingEnabled = true;
            this.lsBoxAvailable.ItemHeight = 14;
            this.lsBoxAvailable.Location = new System.Drawing.Point(12, 37);
            this.lsBoxAvailable.Name = "lsBoxAvailable";
            this.lsBoxAvailable.Size = new System.Drawing.Size(813, 256);
            this.lsBoxAvailable.TabIndex = 6;
            this.lsBoxAvailable.DoubleClick += new System.EventHandler(this.lsBoxAvailable_DoubleClick);
            // 
            // lsBoxMy
            // 
            this.lsBoxMy.Font = new System.Drawing.Font("Consolas", 9F);
            this.lsBoxMy.FormattingEnabled = true;
            this.lsBoxMy.ItemHeight = 14;
            this.lsBoxMy.Location = new System.Drawing.Point(12, 373);
            this.lsBoxMy.Name = "lsBoxMy";
            this.lsBoxMy.Size = new System.Drawing.Size(813, 214);
            this.lsBoxMy.TabIndex = 8;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(291, 11);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 20);
            this.dtpFrom.TabIndex = 9;
            this.dtpFrom.ValueChanged += new System.EventHandler(this.dtpFrom_ValueChanged);
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(542, 11);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 20);
            this.dtpTo.TabIndex = 10;
            this.dtpTo.Value = new System.DateTime(2021, 4, 15, 0, 0, 0, 0);
            this.dtpTo.ValueChanged += new System.EventHandler(this.dtpTo_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(261, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Od:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(512, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Do:";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(671, 604);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(154, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "Zrušit vybranou rezervaci";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnReserve
            // 
            this.btnReserve.Location = new System.Drawing.Point(671, 307);
            this.btnReserve.Name = "btnReserve";
            this.btnReserve.Size = new System.Drawing.Size(154, 23);
            this.btnReserve.TabIndex = 36;
            this.btnReserve.Text = "Zarezervovat vybraný vůz";
            this.btnReserve.UseVisualStyleBackColor = true;
            this.btnReserve.Click += new System.EventHandler(this.btnReserve_Click);
            // 
            // comBoxCar
            // 
            this.comBoxCar.FormattingEnabled = true;
            this.comBoxCar.Location = new System.Drawing.Point(291, 345);
            this.comBoxCar.Name = "comBoxCar";
            this.comBoxCar.Size = new System.Drawing.Size(127, 21);
            this.comBoxCar.TabIndex = 37;
            this.comBoxCar.SelectedIndexChanged += new System.EventHandler(this.comBoxCar_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(244, 348);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Vozidlo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(0, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Uživatel:";
            // 
            // comBoxUser
            // 
            this.comBoxUser.FormattingEnabled = true;
            this.comBoxUser.Location = new System.Drawing.Point(50, 0);
            this.comBoxUser.Name = "comBoxUser";
            this.comBoxUser.Size = new System.Drawing.Size(127, 21);
            this.comBoxUser.TabIndex = 41;
            this.comBoxUser.SelectedIndexChanged += new System.EventHandler(this.comBoxUser_SelectedIndexChanged);
            // 
            // pnlUserReservations
            // 
            this.pnlUserReservations.Controls.Add(this.label6);
            this.pnlUserReservations.Controls.Add(this.comBoxUser);
            this.pnlUserReservations.Location = new System.Drawing.Point(495, 344);
            this.pnlUserReservations.Name = "pnlUserReservations";
            this.pnlUserReservations.Size = new System.Drawing.Size(176, 21);
            this.pnlUserReservations.TabIndex = 42;
            // 
            // btnFuture
            // 
            this.btnFuture.Location = new System.Drawing.Point(134, 604);
            this.btnFuture.Name = "btnFuture";
            this.btnFuture.Size = new System.Drawing.Size(116, 23);
            this.btnFuture.TabIndex = 43;
            this.btnFuture.Text = "Aktuální rezervace";
            this.btnFuture.UseVisualStyleBackColor = true;
            this.btnFuture.Click += new System.EventHandler(this.btnFuture_Click);
            // 
            // btnHistory
            // 
            this.btnHistory.Location = new System.Drawing.Point(256, 604);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(116, 23);
            this.btnHistory.TabIndex = 44;
            this.btnHistory.Text = "Historické rezervace";
            this.btnHistory.UseVisualStyleBackColor = true;
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(12, 604);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(116, 23);
            this.btnAll.TabIndex = 45;
            this.btnAll.Text = "Všechny rezervace";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 639);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.btnHistory);
            this.Controls.Add(this.btnFuture);
            this.Controls.Add(this.pnlUserReservations);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comBoxCar);
            this.Controls.Add(this.btnReserve);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.lsBoxMy);
            this.Controls.Add(this.lsBoxAvailable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblReservations);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Home";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Home_Load);
            this.pnlUserReservations.ResumeLayout(false);
            this.pnlUserReservations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button btnAll;

        private System.Windows.Forms.Button btnFuture;
        private System.Windows.Forms.Button btnHistory;

        private System.Windows.Forms.Label lblReservations;

        private System.Windows.Forms.Button btnReserve;

        private System.Windows.Forms.Button btnDelete;

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lsBoxAvailable;
        private System.Windows.Forms.ListBox lsBoxMy;
        private System.Windows.Forms.ComboBox comBoxCar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comBoxUser;
        private System.Windows.Forms.Panel pnlUserReservations;
    }
}