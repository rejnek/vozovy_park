﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class UserInfo : Form
    {
        private MySqlConnection _conn;
        private readonly int _userId;

        public UserInfo(int userId)
        {
            InitializeComponent();
            _userId = userId;
        }
        private void UserInfo_Load(object sender, EventArgs e)
        {
            _conn = ((Base) this.ParentForm)?.Conn;
            
            // ziskani info o nasem autu
            var sql = $"SELECT * FROM `users` WHERE users.id = '{_userId}'";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do labelu
            while (rdr.Read())
            {
                this.lblUserName.Text = $@"{rdr[5]}";
                this.lblNick.Text = $@"{rdr[3]}";
                this.lblEmail.Text = $@"{rdr[4]}";
                this.lblIdNum.Text = $@"{rdr[6]}";
                this.lblDlNum.Text = $@"{rdr[7]}";
                this.lblBeginDate.Text = $@"{rdr[8]}";
            }

            rdr.Close();

            // ziskani logs
            sql = $"SELECT `time` FROM `logs` WHERE user_id = '{_userId}' ORDER BY `time` DESC";
            rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do lsboxu
            while (rdr.Read())
            {
                this.lsBoxLogs.Items.Add($@"{rdr[0]}");
            }
            rdr.Close();
        }

        private void btnPassword_Click(object sender, EventArgs e)
        {
            // --
            // heslo musi mit minimalne 5 znaku a musi byt jine, nez jsme meli
            // --
            var hash = "";
            var sql = $"SELECT PASSWORD FROM `users` WHERE id='{_userId}'";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();
            if (rdr.Read())
            {
                hash = rdr[0].ToString();
            }
            rdr.Close();
            if (txtNewPass.Text.Length < 5)
            {
                MessageBox.Show(@"Heslo je moc kratke, mimimum je 5 znaku!");
            }
            else if (HashingManager.Verify(this.txtNewPass.Text, hash))
            {
                MessageBox.Show(@"Zadal jsi heslo, ktery uz mas!");
            }
            else
            {
                sql =
                    $"UPDATE `users` SET `password` = '{HashingManager.Hash(this.txtNewPass.Text)}', `new_password` = '0' WHERE `users`.`id` = {_userId}";
                new MySqlCommand(sql, _conn).ExecuteNonQuery();

                // uklizeni po operaci
                ((Base) this.ParentForm)?.pnlMenu.Controls[0].Show();
                ((Base) this.ParentForm)?.ToForm(new UserInfo(this._userId));
                MessageBox.Show(@"Heslo zmeneno.");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var sql =
                $"UPDATE `users` SET `active` = '0', `email` = '', `full_name` = '', `identification` = '0', `driving_licence` = '0', `password` = '' WHERE `users`.`id` = {_userId}";
            new MySqlCommand(sql, _conn).ExecuteNonQuery();

            // uklizeni po operaci
            ((Base) this.ParentForm)?.NewMenu(new User());
            ((Base) this.ParentForm)?.ToForm(new Login());
            MessageBox.Show(@"Uzivatel smazan");
        }

        
    }
}