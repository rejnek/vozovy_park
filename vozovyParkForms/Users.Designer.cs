﻿
namespace vozovyParkForms
{
    partial class Users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxFullName = new System.Windows.Forms.TextBox();
            this.txtBoxNick = new System.Windows.Forms.TextBox();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lsBoxUsers = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.nmrDrivingLc = new System.Windows.Forms.NumericUpDown();
            this.nmrIden = new System.Windows.Forms.NumericUpDown();
            this.btnPasswordChange = new System.Windows.Forms.Button();
            this.chBoxAdmin = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize) (this.nmrDrivingLc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nmrIden)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(228, 567);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Číslo ŘP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(228, 539);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Číslo občanky:";
            // 
            // txtBoxEmail
            // 
            this.txtBoxEmail.Location = new System.Drawing.Point(77, 592);
            this.txtBoxEmail.Name = "txtBoxEmail";
            this.txtBoxEmail.Size = new System.Drawing.Size(136, 20);
            this.txtBoxEmail.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 595);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "E-mail:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 567);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Jméno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 539);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Nickname:";
            // 
            // txtBoxFullName
            // 
            this.txtBoxFullName.Location = new System.Drawing.Point(77, 566);
            this.txtBoxFullName.Name = "txtBoxFullName";
            this.txtBoxFullName.Size = new System.Drawing.Size(136, 20);
            this.txtBoxFullName.TabIndex = 26;
            // 
            // txtBoxNick
            // 
            this.txtBoxNick.Location = new System.Drawing.Point(77, 539);
            this.txtBoxNick.Name = "txtBoxNick";
            this.txtBoxNick.Size = new System.Drawing.Size(136, 20);
            this.txtBoxNick.TabIndex = 25;
            // 
            // btnAddUser
            // 
            this.btnAddUser.Location = new System.Drawing.Point(459, 563);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(97, 20);
            this.btnAddUser.TabIndex = 24;
            this.btnAddUser.Text = "Přidat uživatele";
            this.btnAddUser.UseVisualStyleBackColor = true;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label2.Location = new System.Drawing.Point(10, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 25);
            this.label2.TabIndex = 22;
            this.label2.Text = "Uživatelé v systému:";
            // 
            // lsBoxUsers
            // 
            this.lsBoxUsers.Font = new System.Drawing.Font("Consolas", 9F);
            this.lsBoxUsers.FormattingEnabled = true;
            this.lsBoxUsers.ItemHeight = 14;
            this.lsBoxUsers.Location = new System.Drawing.Point(19, 32);
            this.lsBoxUsers.Name = "lsBoxUsers";
            this.lsBoxUsers.Size = new System.Drawing.Size(823, 466);
            this.lsBoxUsers.TabIndex = 23;
            this.lsBoxUsers.DoubleClick += new System.EventHandler(this.lsBoxUsers_DoubleClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(228, 595);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Heslo:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(305, 592);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(136, 20);
            this.txtPassword.TabIndex = 37;
            // 
            // nmrDrivingLc
            // 
            this.nmrDrivingLc.Location = new System.Drawing.Point(305, 565);
            this.nmrDrivingLc.Maximum = new decimal(new int[] {1000000000, 0, 0, 0});
            this.nmrDrivingLc.Name = "nmrDrivingLc";
            this.nmrDrivingLc.Size = new System.Drawing.Size(135, 20);
            this.nmrDrivingLc.TabIndex = 38;
            // 
            // nmrIden
            // 
            this.nmrIden.Location = new System.Drawing.Point(305, 537);
            this.nmrIden.Maximum = new decimal(new int[] {1410065408, 2, 0, 0});
            this.nmrIden.Name = "nmrIden";
            this.nmrIden.Size = new System.Drawing.Size(135, 20);
            this.nmrIden.TabIndex = 39;
            // 
            // btnPasswordChange
            // 
            this.btnPasswordChange.Location = new System.Drawing.Point(726, 523);
            this.btnPasswordChange.Name = "btnPasswordChange";
            this.btnPasswordChange.Size = new System.Drawing.Size(116, 23);
            this.btnPasswordChange.TabIndex = 40;
            this.btnPasswordChange.Text = "Vyžádat změnu hesla";
            this.btnPasswordChange.UseVisualStyleBackColor = true;
            this.btnPasswordChange.Click += new System.EventHandler(this.btnPasswordChange_Click);
            // 
            // chBoxAdmin
            // 
            this.chBoxAdmin.Location = new System.Drawing.Point(459, 534);
            this.chBoxAdmin.Name = "chBoxAdmin";
            this.chBoxAdmin.Size = new System.Drawing.Size(136, 24);
            this.chBoxAdmin.TabIndex = 41;
            this.chBoxAdmin.Text = "Administrátorská práva";
            this.chBoxAdmin.UseVisualStyleBackColor = true;
            // 
            // Users
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 639);
            this.Controls.Add(this.chBoxAdmin);
            this.Controls.Add(this.btnPasswordChange);
            this.Controls.Add(this.nmrIden);
            this.Controls.Add(this.nmrDrivingLc);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBoxEmail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBoxFullName);
            this.Controls.Add(this.txtBoxNick);
            this.Controls.Add(this.btnAddUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lsBoxUsers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Users";
            this.Text = "Uživatelé";
            this.Load += new System.EventHandler(this.Users_Load);
            ((System.ComponentModel.ISupportInitialize) (this.nmrDrivingLc)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nmrIden)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.CheckBox chBoxAdmin;

        private System.Windows.Forms.Button btnPasswordChange;

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxFullName;
        private System.Windows.Forms.TextBox txtBoxNick;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lsBoxUsers;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.NumericUpDown nmrDrivingLc;
        private System.Windows.Forms.NumericUpDown nmrIden;
    }
}