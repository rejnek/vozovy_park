﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class Login : Form
    {
        MySqlConnection _conn;
        private User u;

        public Login()
        {
            InitializeComponent();
            u = new User();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            //input
            var nickname = txtNickname.Text;
            var password = txtPassword.Text;

            // ziskani hesla
            var sql = "SELECT ACTIVE, PASSWORD FROM `users` WHERE nickname='" + nickname + "'";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // prihlaseni
            if (rdr.Read() && (bool) rdr[0] && HashingManager.Verify(password,rdr[1].ToString()))
            {
                rdr.Close();
                // vytvoreni uctu
                var sqlU = "SELECT `id`, `admin`, `full_name`, `new_password` FROM `users` WHERE nickname='" + nickname + "'";
                var rdrU = new MySqlCommand(sqlU, _conn).ExecuteReader();
                if (rdrU.Read())
                {
                    u = new User((int) rdrU[0], (bool) rdrU[1], true, rdrU[2].ToString());
                    ((Base) this.ParentForm)?.NewMenu(u);
                    if ((bool)rdrU[3])
                    {
                        rdrU.Close();
                        ((Base) this.ParentForm)?.pnlMenu.Controls[0].Hide();
                        ((Base) this.ParentForm)?.ToForm(new UserInfo(u.id));
                        MessageBox.Show(@"Musis si zmenit heslo!");
                    }
                    else
                    {
                        rdrU.Close();
                        ((Base) this.ParentForm)?.ToForm(new Home(u));
                    }
                }
                rdrU.Close();
                this.WriteLog(u);
            }
            else
            {
                rdr.Close();
                MessageBox.Show(@"Spatna kombinace jmena a hesla.");
            }
        }

        private void login_Load(object sender, EventArgs e)
        {
            try
            {
                _conn = ((Base) this.ParentForm)?.Conn;;
            }
            catch (Exception error)
            {
                MessageBox.Show(@"Nepodarilo se pripojit k databazovemu serveru. Zkuste se pripojit k internetu.");
                login_Load(null, EventArgs.Empty);
            }
        }

        private void WriteLog(User u)
        {
            var sql = $"INSERT INTO `logs` (`id`, `user_id`, `time`) VALUES (NULL, '{u.id}', NOW())";
            new MySqlCommand(sql, _conn).ExecuteNonQuery();
            
        }

        private void NewPassword(User u)
        {
            MessageBox.Show(@"Je nutne si zmenit heslo!");
            ((Base) this.ParentForm)?.ToForm(new Home(u));
        }
    }
}