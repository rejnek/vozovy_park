﻿namespace vozovyParkForms
{
    public class User
    {
        public int id;
        public bool isAdmin, loggedIn;
        public string fullName;

        public User(int id, bool isAdmin, bool loggedIn, string fullName)
        {
            this.id = id;
            this.isAdmin = isAdmin;
            this.loggedIn = loggedIn;
            this.fullName = fullName;
        }

        public User(User u) : this(u.id, u.isAdmin, u.loggedIn, u.fullName)
        {
        }

        public User() : this(0, false, false, "nepřihlášený uživatel")
        {
        }
    }
}