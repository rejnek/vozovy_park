﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class Users : Form
    {
        private MySqlConnection _conn;
        private readonly List<int> _usersId;

        public Users()
        {
            InitializeComponent();
            _usersId = new List<int>();
        }

        private void Users_Load(object sender, EventArgs e)
        {
            _conn = ((Base) this.ParentForm)?.Conn;

            // ziskani vsech uzivatelu
            const string sql = "SELECT * FROM `users` WHERE `users`.`active`";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do listboxu
            while (rdr.Read())
            {
                lsBoxUsers.Items.Add(
                    $"{rdr[1]}\t\t{rdr[2]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}\t\t{rdr[7]}\t\t{rdr[8]}");
                _usersId.Add((int) rdr[0]);
            }

            rdr.Close();
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            // --
            // moznost pridat kontrolu vstupu - obcanka, ridicak
            // --
            if (IsValidEmail(txtBoxEmail.Text) == false)
            {
                MessageBox.Show(@"Email je neplatny.");
            }
            else if (txtPassword.Text.Length < 5)
            {
                MessageBox.Show(@"Heslo je moc kratky.");
            }
            else
            {
                try
                {
                    var sql =
                        $"INSERT INTO `users` (`id`, `active`, `admin` ,`nickname`, `email`, `full_name`, `identification`, `driving_licence`, `created_at`, `password`, `new_password`) VALUES (NULL, '1','{Convert.ToByte(chBoxAdmin.Checked)}' , '{txtBoxNick.Text}', '{txtBoxEmail.Text}', '{txtBoxFullName.Text}', '{nmrIden.Value}', '{nmrDrivingLc.Value}', NOW(), '{HashingManager.Hash(txtPassword.Text)}', '0')";
                    new MySqlCommand(sql, _conn).ExecuteNonQuery();

                    // uklizeni po operaci
                    ((Base) this.ParentForm)?.ToForm(new Users());
                    MessageBox.Show(@"Uzivatel uspesne pridan.");
                }
                catch (MySqlException err)
                {
                    MessageBox.Show(@"Uzivatel se stenym nickname/obcanskym prukazem/ridicskym prukazem uz existuje." + err.Message);
                }
            }
        }

        private void lsBoxUsers_DoubleClick(object sender, EventArgs e)
        {
            if (lsBoxUsers.SelectedItem != null)
            {
                // otevreni noveho okna Car Info
                ((Base) this.ParentForm)?.ToForm(new UserInfo(_usersId[lsBoxUsers.SelectedIndex]));
            }
        }

        private void btnPasswordChange_Click(object sender, EventArgs e)
        {
            var sql =
                $"UPDATE `users` SET `new_password` = '1' WHERE `users`.`id` = {_usersId[lsBoxUsers.SelectedIndex]}";
            new MySqlCommand(sql, _conn).ExecuteNonQuery();
            MessageBox.Show(@"Uzivatel vyzvan ke zmene hesla.");
        }

        // --
        // Metoda pouzita z:
        // https://stackoverflow.com/questions/5342375/regex-email-validation
        // --
        private bool IsValidEmail(string emailaddress)
        {
            try
            {
                var m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}