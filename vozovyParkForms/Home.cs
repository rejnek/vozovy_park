﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class Home : Form
    {
        private MySqlConnection _conn;
        private List<int> _myReservations, _avCars, _cmbCars, _cmbUsers;
        private readonly User _user;
        private string _sqlTimeSpecification;
        private string _lastFilling;

        public Home(User u)
        {
            InitializeComponent();
            _user = new User(u);
            // _conn = Db.Connect();
        }
        

        private void lsBoxAvailable_DoubleClick(object sender, EventArgs e)
        {
            if (lsBoxAvailable.SelectedItem != null)
            {
                // otevreni noveho okna Car Info
                ((Base) this.ParentForm)?.ToForm(new CarInfo(_avCars[lsBoxAvailable.SelectedIndex], _user));
            }
        }

        private void FillListAvailable()
        {
            this._avCars = new List<int>();
            this.lsBoxAvailable.Items.Clear();

            // ziskani nezarezervovanych aut
            var sql = @$"
            SELECT
                `cars`.*
            FROM
                `cars`
            LEFT JOIN(
                SELECT
                    `cars`.`id`
            FROM
                `cars`
            INNER JOIN `reservations` ON `reservations`.`car_id` = `cars`.`id`
            WHERE
                ((
                `reservations`.`begin` BETWEEN '{this.dtpFrom.Value:yyyy-MM-dd HH:mm:ss.fff}' AND '{this.dtpTo.Value:yyyy-MM-dd HH:mm:ss.fff}'
                ) OR(
                `reservations`.`end` BETWEEN '{this.dtpFrom.Value:yyyy-MM-dd HH:mm:ss.fff}' AND '{this.dtpTo.Value:yyyy-MM-dd HH:mm:ss.fff}'
                ) OR(
                `reservations`.`begin` <= '{this.dtpFrom.Value:yyyy-MM-dd HH:mm:ss.fff}' AND `reservations`.`end` >= '{this.dtpTo.Value:yyyy-MM-dd HH:mm:ss.fff}'
                ))
                AND(
                `reservations`.`active`
                )
            GROUP BY
                `cars`.`id`
                ) AS x
            ON
                `cars`.`id` = `x`.`id`
            WHERE
                `x`.`id` IS NULL AND `cars`.`active`;";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do listboxu
            while (rdr.Read())
            {
                this.lsBoxAvailable.Items.Add($"{rdr[2]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}");
                _avCars.Add((int) rdr[0]);
            }

            rdr.Close();
        }

        private void FillListMy()
        {
            _myReservations = new List<int>();
            this.lsBoxMy.Items.Clear();
            if (_user.isAdmin == false)
            {
                // ziskani mojich rezervaci
                var sql =
                    @$"SELECT * FROM `cars_reservations` WHERE cars_reservations.user_id = '{_user.id}' AND cars_reservations.rs_active AND cars_reservations.car_active AND cars_reservations.end {_sqlTimeSpecification}";
                var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

                // jejich vypsani do listboxu
                while (rdr.Read())
                {
                    this.lsBoxMy.Items.Add($"{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}\t\t{rdr[7]}");
                    _myReservations.Add((int) rdr[1]);
                }

                rdr.Close();
            }
            else
            {
                this.lblReservations.Text = @"Všechny rezervace:";

                // ziskani vsech rezervaci
                var sql =
                    @$"SELECT * FROM `cars_reservations` WHERE cars_reservations.rs_active AND cars_reservations.car_active AND cars_reservations.end {_sqlTimeSpecification}";
                var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

                // jejich vypsani do listboxu
                while (rdr.Read())
                {
                    this.lsBoxMy.Items.Add($"{rdr[9]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}\t\t{rdr[7]}");
                    _myReservations.Add((int) rdr[1]);
                }

                rdr.Close();
            }

            _sqlTimeSpecification = "> '1970-01-01 00:00:00.000'";
            _lastFilling = "all";
        }

        private void FillComBoxCars()
        {
            // ziskani vsech rezervaci
            var sql =
                @$"SELECT `id`, `brand`, `model` FROM `cars`";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();
            _cmbCars = new List<int>();

            // jejich vypsani do listboxu
            while (rdr.Read())
            {
                this.comBoxCar.Items.Add($"{rdr[1]} {rdr[2]}");
                _cmbCars.Add((int) rdr[0]);
            }

            rdr.Close();
        }

        private void FillComboUsers()
        {
            // ziskani vsech rezervaci
            var sql =
                @$"SELECT `id`, `full_name` FROM `users` WHERE `users`.`active`";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();
            _cmbUsers = new List<int>();

            // jejich vypsani do listboxu
            while (rdr.Read())
            {
                this.comBoxUser.Items.Add($"{rdr[1]}");
                _cmbUsers.Add((int) rdr[0]);
            }

            rdr.Close();
        }

        private void dtpFrom_ValueChanged(object sender, EventArgs e)
        {
            // refresh home
            this.FillListAvailable();
        }

        private void dtpTo_ValueChanged(object sender, EventArgs e)
        {
            // refresh home
            this.FillListAvailable();
        }

        private void comBoxUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lsBoxMy.Items.Clear();
            if (this.comBoxUser.SelectedIndex == -1)
            {
                this.FillListMy();
            }
            else
            {
                this.lblReservations.Text = @"Rezervace od uživatele:";
                _myReservations = new List<int>();

                // ziskani vsech rezervaci
                var sql =
                    @$"SELECT * FROM `cars_reservations` WHERE cars_reservations.rs_active AND cars_reservations.car_active AND cars_reservations.user_id = '{_cmbUsers[this.comBoxUser.SelectedIndex]}' AND cars_reservations.end {_sqlTimeSpecification}";
                var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

                // jejich vypsani do listboxu
                while (rdr.Read())
                {
                    this.lsBoxMy.Items.Add($"{rdr[9]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}\t\t{rdr[7]}");
                    _myReservations.Add((int) rdr[1]);
                }

                rdr.Close();
            }

            _sqlTimeSpecification = "> '1970-01-01 00:00:00.000'";
            _lastFilling = "user";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lsBoxMy.SelectedItem == null) return;

            var sql =
                $"UPDATE `cars_reservations` SET `rs_active` = '0' WHERE `cars_reservations`.`id` = '{_myReservations[this.lsBoxMy.SelectedIndex]}'";
            new MySqlCommand(sql, _conn).ExecuteNonQuery();

            // uklizeni po operaci
            this.FillListAvailable();
            this.FillListMy();
            this.comBoxCar.SelectedIndex = 0;
            this.comBoxUser.SelectedIndex = 0;
            MessageBox.Show(@"Auto odrezervovano.");
        }

        private void comBoxCar_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lsBoxMy.Items.Clear();
            if (this.comBoxCar.SelectedIndex == -1)
            {
                this.FillListMy();
            }
            else
            {
                this.lblReservations.Text = @"Rezervace auta:";
                _myReservations = new List<int>();
                var userId = "";
                if (_user.isAdmin == false)
                {
                    userId = $"AND cars_reservations.user_id = '{_user.id}'";
                }

                // ziskani vsech rezervaci
                var sql =
                    @$"SELECT * FROM `cars_reservations` WHERE cars_reservations.rs_active AND cars_reservations.car_active AND cars_reservations.car_id = '{_cmbCars[this.comBoxCar.SelectedIndex]}' {userId} AND cars_reservations.end {_sqlTimeSpecification}";
                var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

                // jejich vypsani do listboxu
                while (rdr.Read())
                {
                    this.lsBoxMy.Items.Add($"{rdr[9]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}\t\t{rdr[7]}");
                    _myReservations.Add((int) rdr[1]);
                }

                rdr.Close();
            }

            _sqlTimeSpecification = "> '1970-01-01 00:00:00.000'";
            _lastFilling = "car";
        }

        private void btnReserve_Click(object sender, EventArgs e)
        {
            // --
            // Kontrola vstupu
            // --
            if (this.dtpFrom.Value > this.dtpTo.Value)
            {
                MessageBox.Show(@"Zacatek rezervace musi byt pred jejim koncem.");
            }
            else if (this.dtpFrom.Value < DateTime.Now || this.dtpTo.Value < DateTime.Now)
            {
                MessageBox.Show(@"Rezervace nemuze byt v minulosti");
            }
            else
            {
                if (lsBoxAvailable.SelectedIndex != -1)
                {
                    var sql =
                        $"INSERT INTO `reservations` (`id`, `user_id`, `car_id`, `active`, `begin`, `end`, `notes`) VALUES (NULL, '{_user.id}', '{_avCars[lsBoxAvailable.SelectedIndex]}', TRUE, '{this.dtpFrom.Value:yyyy-MM-dd HH:mm:ss.fff}', '{this.dtpTo.Value:yyyy-MM-dd HH:mm:ss.fff}', NULL)";
                    new MySqlCommand(sql, _conn).ExecuteNonQuery();

                    // uklizeni po operaci
                    this.FillListAvailable();
                    this.FillListMy();
                    MessageBox.Show(@"Rezervace uspesne pridana.");
                }
                else
                {
                    MessageBox.Show(@"Musi se vybrat auto.");
                }
            }
        }

        private void btnFuture_Click(object sender, EventArgs e)
        {
            _sqlTimeSpecification = "> NOW()";
            Fill();
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            _sqlTimeSpecification = "< NOW()";
            Fill();
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            _sqlTimeSpecification = "> '1970-01-01 00:00:00.000'";
            Fill();
        }
        private void Fill()
        {
            switch (_lastFilling)
            {
                case "all":
                    this.FillListMy();
                    break;
                case "user":
                    this.comBoxUser_SelectedIndexChanged(null, EventArgs.Empty);
                    break;
                case "car":
                    this.comBoxCar_SelectedIndexChanged(null, EventArgs.Empty);
                    break;
            }
        }

        private void Home_Load(object sender, EventArgs e)
        {
            _conn = ((Base) this.ParentForm)?.Conn;
            _sqlTimeSpecification = "> '1970-01-01 00:00:00.000'";
            this.FillListAvailable();
            this.FillListMy();
            this.FillComBoxCars();
            this.FillComboUsers();
            if (_user.isAdmin == false)
            {
                this.pnlUserReservations.Hide();
            }
        }
    }
}