﻿
namespace vozovyParkForms
{
    partial class Cars
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.lsBoxAvailable = new System.Windows.Forms.ListBox();
            this.btnNewCar = new System.Windows.Forms.Button();
            this.txtBoxBrand = new System.Windows.Forms.TextBox();
            this.txtBoxModel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxType = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.nmrConsump = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxNotes = new System.Windows.Forms.RichTextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nmrConsump)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Dostupné rezervace:";
            // 
            // lsBoxAvailable
            // 
            this.lsBoxAvailable.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lsBoxAvailable.FormattingEnabled = true;
            this.lsBoxAvailable.ItemHeight = 14;
            this.lsBoxAvailable.Location = new System.Drawing.Point(22, 37);
            this.lsBoxAvailable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lsBoxAvailable.Name = "lsBoxAvailable";
            this.lsBoxAvailable.Size = new System.Drawing.Size(959, 550);
            this.lsBoxAvailable.TabIndex = 6;
            this.lsBoxAvailable.DoubleClick += new System.EventHandler(this.lsBoxAvailable_DoubleClick);
            // 
            // btnNewCar
            // 
            this.btnNewCar.Location = new System.Drawing.Point(629, 683);
            this.btnNewCar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewCar.Name = "btnNewCar";
            this.btnNewCar.Size = new System.Drawing.Size(113, 27);
            this.btnNewCar.TabIndex = 10;
            this.btnNewCar.Text = "Přidat nové auto";
            this.btnNewCar.UseVisualStyleBackColor = true;
            this.btnNewCar.Click += new System.EventHandler(this.btnNewCar_Click);
            // 
            // txtBoxBrand
            // 
            this.txtBoxBrand.Location = new System.Drawing.Point(74, 618);
            this.txtBoxBrand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtBoxBrand.Name = "txtBoxBrand";
            this.txtBoxBrand.Size = new System.Drawing.Size(158, 23);
            this.txtBoxBrand.TabIndex = 12;
            // 
            // txtBoxModel
            // 
            this.txtBoxModel.Location = new System.Drawing.Point(74, 651);
            this.txtBoxModel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtBoxModel.Name = "txtBoxModel";
            this.txtBoxModel.Size = new System.Drawing.Size(158, 23);
            this.txtBoxModel.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 622);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "Značka:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 654);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "Model:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 687);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Typ:";
            // 
            // txtBoxType
            // 
            this.txtBoxType.Location = new System.Drawing.Point(74, 683);
            this.txtBoxType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtBoxType.Name = "txtBoxType";
            this.txtBoxType.Size = new System.Drawing.Size(158, 23);
            this.txtBoxType.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(266, 624);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 15);
            this.label4.TabIndex = 18;
            this.label4.Text = "Spotřeba:";
            // 
            // nmrConsump
            // 
            this.nmrConsump.DecimalPlaces = 2;
            this.nmrConsump.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nmrConsump.Location = new System.Drawing.Point(337, 622);
            this.nmrConsump.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.nmrConsump.Name = "nmrConsump";
            this.nmrConsump.Size = new System.Drawing.Size(71, 23);
            this.nmrConsump.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(266, 657);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 15);
            this.label5.TabIndex = 20;
            this.label5.Text = "Poznámky:";
            // 
            // txtBoxNotes
            // 
            this.txtBoxNotes.Location = new System.Drawing.Point(337, 651);
            this.txtBoxNotes.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtBoxNotes.Name = "txtBoxNotes";
            this.txtBoxNotes.Size = new System.Drawing.Size(284, 57);
            this.txtBoxNotes.TabIndex = 21;
            this.txtBoxNotes.Text = "";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(863, 603);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(119, 27);
            this.btnDelete.TabIndex = 22;
            this.btnDelete.Text = "Smazat vybrané auto";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Cars
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 737);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtBoxNotes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nmrConsump);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBoxType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBoxModel);
            this.Controls.Add(this.txtBoxBrand);
            this.Controls.Add(this.btnNewCar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lsBoxAvailable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.Name = "Cars";
            this.Text = "uživatelé";
            this.Load += new System.EventHandler(this.Cars_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nmrConsump)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button btnDelete;

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lsBoxAvailable;
        private System.Windows.Forms.Button btnNewCar;
        private System.Windows.Forms.TextBox txtBoxBrand;
        private System.Windows.Forms.TextBox txtBoxModel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nmrConsump;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox txtBoxNotes;
    }
}