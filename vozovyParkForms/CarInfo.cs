﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vozovyParkForms
{
    public partial class CarInfo : Form
    {
        private MySqlConnection _conn;
        private readonly int _carId;
        private User _u;

        public CarInfo(int carId, User u)
        {
            InitializeComponent();
            _carId = carId;
            _u = new User(u);
        }
        private void CarInfo_Load(object sender, EventArgs e)
        {
            _conn = ((Base) this.ParentForm)?.Conn;
            
            // ziskani info o nasem autu
            var sql = $"SELECT * FROM `cars` WHERE cars.id = '{_carId}'";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do labelu
            while (rdr.Read())
            {
                this.lblCarName.Text = $@"{rdr[2]} {rdr[3]}";
                this.lblType.Text = $@"{rdr[4]}";
                this.lblConsump.Text = $@"{rdr[5]} l/100km";
                this.lblNotes.Text = $@"{rdr[6]}";
            }
            rdr.Close();
            FillServis(_carId);

            if (_u.isAdmin == false)
            {
                this.pnlServis.Hide();
            }
            
        }
        private void FillServis(int carId)
        {
            // ziskani info o nasem autu 2- 6
            var sql = $"SELECT * FROM `cars_service` WHERE cars_service.car_id = '{carId}'";
            var rdr = new MySqlCommand(sql, _conn).ExecuteReader();

            // jejich vypsani do lsBoxu
            while (rdr.Read())
            {
                this.lsBoxHistory.Items.Add($"{rdr[2]}\t\t{rdr[3]}\t\t{rdr[4]}\t\t{rdr[5]}\t\t{rdr[6]}");
            }
            rdr.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var sql = $"INSERT INTO `cars_service` (`id`, `car_id`, `repair_name`, `repair_desc`, `time`, `invoice`, `cost`) VALUES (NULL, '{_carId}', '{txtName.Text}', '{txtNotes.Text}', '{dtpDate.Value:yyyy-MM-dd HH:mm:ss.fff}', '{nmrInv.Value}', '{nmrPrice.Value}')";
            new MySqlCommand(sql, _conn).ExecuteNonQuery();

            // uklizeni po operaci
            ((Base)this.ParentForm)?.ToForm(new CarInfo(_carId, _u));
            MessageBox.Show(@"Servisni ukon uspesne pridan do databaze.");
        }
    }
}