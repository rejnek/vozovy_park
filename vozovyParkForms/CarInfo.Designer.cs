﻿
namespace vozovyParkForms
{
    partial class CarInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCarName = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblNotes = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblConsump = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lsBoxHistory = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtNotes = new System.Windows.Forms.RichTextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.nmrInv = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nmrPrice = new System.Windows.Forms.NumericUpDown();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlServis = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize) (this.nmrInv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nmrPrice)).BeginInit();
            this.pnlServis.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCarName
            // 
            this.lblCarName.AutoSize = true;
            this.lblCarName.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.lblCarName.Location = new System.Drawing.Point(10, 8);
            this.lblCarName.Name = "lblCarName";
            this.lblCarName.Size = new System.Drawing.Size(67, 25);
            this.lblCarName.TabIndex = 5;
            this.lblCarName.Text = "Jméno";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(106, 39);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(29, 13);
            this.lblType.TabIndex = 23;
            this.lblType.Text = "label";
            // 
            // lblNotes
            // 
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(106, 85);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(64, 13);
            this.lblNotes.TabIndex = 24;
            this.lblNotes.Text = "poznamky...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Typ vozidla:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Spotřeba:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Poznámky:";
            // 
            // lblConsump
            // 
            this.lblConsump.AutoSize = true;
            this.lblConsump.Location = new System.Drawing.Point(106, 62);
            this.lblConsump.Name = "lblConsump";
            this.lblConsump.Size = new System.Drawing.Size(61, 13);
            this.lblConsump.TabIndex = 28;
            this.lblConsump.Text = "0.0l/100km";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label10.Location = new System.Drawing.Point(10, 118);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 25);
            this.label10.TabIndex = 29;
            this.label10.Text = "Historie servisu";
            // 
            // lsBoxHistory
            // 
            this.lsBoxHistory.Font = new System.Drawing.Font("Consolas", 9F);
            this.lsBoxHistory.FormattingEnabled = true;
            this.lsBoxHistory.ItemHeight = 14;
            this.lsBoxHistory.Location = new System.Drawing.Point(35, 146);
            this.lsBoxHistory.Name = "lsBoxHistory";
            this.lsBoxHistory.Size = new System.Drawing.Size(802, 368);
            this.lsBoxHistory.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Datum:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Popis:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Jméno opravy:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label12.Location = new System.Drawing.Point(1, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 25);
            this.label12.TabIndex = 32;
            this.label12.Text = "Nový servis";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 29);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(138, 20);
            this.txtName.TabIndex = 39;
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(107, 55);
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(138, 51);
            this.txtNotes.TabIndex = 40;
            this.txtNotes.Text = "";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(319, 29);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(267, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Faktura:";
            // 
            // nmrInv
            // 
            this.nmrInv.Location = new System.Drawing.Point(319, 55);
            this.nmrInv.Maximum = new decimal(new int[] {1410065408, 2, 0, 0});
            this.nmrInv.Name = "nmrInv";
            this.nmrInv.Size = new System.Drawing.Size(135, 20);
            this.nmrInv.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(267, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "Cena:";
            // 
            // nmrPrice
            // 
            this.nmrPrice.Location = new System.Drawing.Point(319, 85);
            this.nmrPrice.Maximum = new decimal(new int[] {1410065408, 2, 0, 0});
            this.nmrPrice.Name = "nmrPrice";
            this.nmrPrice.Size = new System.Drawing.Size(135, 20);
            this.nmrPrice.TabIndex = 45;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(531, 85);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(80, 23);
            this.btnAdd.TabIndex = 46;
            this.btnAdd.Text = "Přidat servis";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // pnlServis
            // 
            this.pnlServis.Controls.Add(this.btnAdd);
            this.pnlServis.Controls.Add(this.label12);
            this.pnlServis.Controls.Add(this.label8);
            this.pnlServis.Controls.Add(this.nmrPrice);
            this.pnlServis.Controls.Add(this.label7);
            this.pnlServis.Controls.Add(this.label5);
            this.pnlServis.Controls.Add(this.label6);
            this.pnlServis.Controls.Add(this.nmrInv);
            this.pnlServis.Controls.Add(this.txtName);
            this.pnlServis.Controls.Add(this.label4);
            this.pnlServis.Controls.Add(this.txtNotes);
            this.pnlServis.Controls.Add(this.dtpDate);
            this.pnlServis.Location = new System.Drawing.Point(10, 518);
            this.pnlServis.Name = "pnlServis";
            this.pnlServis.Size = new System.Drawing.Size(826, 113);
            this.pnlServis.TabIndex = 47;
            // 
            // CarInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 639);
            this.Controls.Add(this.pnlServis);
            this.Controls.Add(this.lsBoxHistory);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblConsump);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNotes);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.lblCarName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "CarInfo";
            this.Text = "CarInfo";
            this.Load += new System.EventHandler(this.CarInfo_Load);
            ((System.ComponentModel.ISupportInitialize) (this.nmrInv)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nmrPrice)).EndInit();
            this.pnlServis.ResumeLayout(false);
            this.pnlServis.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nmrInv;
        private System.Windows.Forms.NumericUpDown nmrPrice;

        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.RichTextBox txtNotes;

        private System.Windows.Forms.TextBox txtName;

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;

        private System.Windows.Forms.ListBox lsBoxHistory;

        private System.Windows.Forms.Label label10;

        #endregion
        private System.Windows.Forms.Label lblCarName;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblConsump;
        private System.Windows.Forms.Panel pnlServis;
    }
}